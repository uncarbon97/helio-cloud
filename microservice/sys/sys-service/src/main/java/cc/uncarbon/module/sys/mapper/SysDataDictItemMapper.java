package cc.uncarbon.module.sys.mapper;

import cc.uncarbon.module.sys.entity.SysDataDictItemEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


/**
 * 数据字典项
 */
@Mapper
public interface SysDataDictItemMapper extends BaseMapper<SysDataDictItemEntity> {

}
